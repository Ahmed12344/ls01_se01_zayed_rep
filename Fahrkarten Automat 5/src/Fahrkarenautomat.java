import java.util.Scanner;

public class Fahrkarenautomat {

	int anzahlTickets;
	double ticketPreis;
	char antwort = 'N';
	Scanner tastatur = new Scanner(System.in);
	{

		do {
			System.out.println("Ticketpreis (EURO-Cent): ");
			ticketPreis = tastatur.nextDouble();

			System.out.println("Wie viele Karten wollen Sie haben (Maximal 10 sind erlaubt)");
			anzahlTickets = tastatur.nextInt();

			if (anzahlTickets <= 10 && anzahlTickets > 0) {
				System.out.println("Sie bestellen " + anzahlTickets + " Karten");
			} else {
				System.out.println("SYSTEM FEHLER-Falsche Eingabe");
			}
			System.out.println("\nM�chten sie die Eingabe best�tigen? Y/N");
			antwort = tastatur.next().charAt(0);

		} while (antwort == 'N');
	}
}